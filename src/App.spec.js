import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Store from './redux/store';
import App from './App';

const defaultState = {
  currentDisplay: {
    url: ''
  }
}
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={Store(defaultState)}><App /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
