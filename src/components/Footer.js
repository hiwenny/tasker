import React from 'react'
import { string } from 'prop-types'

const FooterComponent = props => {
  const { text } = props

  return (
    <div className="footer">{text}</div>
  )
}
FooterComponent.propTypes = {
  text: string
}

export default FooterComponent