// Absolute imports
import React from 'react'
import { eventsLabel } from '../../utils/constants'
import ProfileLabel from './ProfileLabel';
import TaskLabel from './TaskLabel';


const ListItemsComponent = ({data, onMouseEnter, onMouseLeave}) => {
  return (
    <div className="list-items">
      <ul>
        {data.activity_feed.map((activity, i) => 
          <li key={i}>
            {activity.profile_ids[0] &&
              <ProfileLabel 
                profileId={activity.profile_ids[0]}
                profiles={data.profiles} 
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
              />
            }
            <span className="">{` ${eventsLabel[activity.event]} `}</span>
            {
              activity.task_id && 
                <TaskLabel 
                  taskId={activity.task_id}
                  tasks={data.tasks}
                  onMouseEnter={onMouseEnter}
                  onMouseLeave={onMouseLeave}
                />
            }
            {
              activity.event === 'assigned' && ([
                <span key={0}> TO </span>,
                <ProfileLabel
                  key={1}
                  profileId={activity.profile_ids[1]}
                  profiles={data.profiles}
                  onMouseEnter={onMouseEnter}
                  onMouseLeave={onMouseLeave}
                />
              ])
            }
          </li>)
        }
      </ul>
    </div>
  )
}
// ListItemsComponent.propTypes = {
//   text: string
// }

export default ListItemsComponent