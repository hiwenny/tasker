import React from 'react'
import { number, array } from 'prop-types'
import { composeTaskSlug } from '../../utils/labels'
import { getTaskSlugFromId, getTaskNameFromId } from '../../utils/mapper'


const TaskLabel = ({taskId, tasks, onMouseEnter, onMouseLeave}) => {
  return (
    <a className="label"
      href={composeTaskSlug(getTaskSlugFromId(taskId, tasks))}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}>
      {getTaskNameFromId(taskId, tasks)}
    </a>
  )
}
TaskLabel.propTypes = {
  taskId: number.isRequired,
  tasks: array.isRequired
}

export default TaskLabel