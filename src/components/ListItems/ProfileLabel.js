import React from 'react'
import { number, array } from 'prop-types'
import { composeUserSlug } from '../../utils/labels'
import { getProfileNameFromId, getProfileSlugFromId } from '../../utils/mapper'
import { LabelStyles } from '../../css/components.css'

const ProfileLabel = ({profileId, profiles, onMouseEnter, onMouseLeave}) => {
  return (
    <a className="label"
      href={composeUserSlug(getProfileSlugFromId(profileId, profiles))}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      style={LabelStyles}
    >
      {getProfileNameFromId(profileId, profiles)}
    </a>
  )
}
ProfileLabel.propTypes = {
  profileId: number.isRequired,
  profiles: array.isRequired
}

export default ProfileLabel