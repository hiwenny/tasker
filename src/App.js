import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import axios from 'axios'
import { updateUrl, fetchDataSuccess } from './redux/actions'
import ListItems from './components/ListItems'
import Footer from './components/Footer'

class App extends Component {
  urlUpdater = (e) => this.props.updateUrlAction(e.target.attributes.href.value)
  urlResetter = () => this.props.updateUrlAction('')

  componentDidMount() {
    // TODO: put initial fetch in a preload/initial setup so the activityfeed check isn't required.
    this.getStories()
  }

  getStories = () => {
    // TODO: move this to .env or some config
    const BASE_URL = 'http://localhost:3001'
    const TEMP_API = `${BASE_URL}/api/mocks`
    axios.get(TEMP_API)
      .then(result => this.props.fetchDataSuccessAction(result.data))
      .catch(error => console.error(error));
  }

  render() {
    return (
      <div className="App">
        {this.props.activityFeed && <ListItems data={this.props.activityFeed} onMouseEnter={this.urlUpdater} onMouseLeave={this.urlResetter} />}
        <Footer text={this.props.currentDisplay.url} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
      currentDisplay: state.currentDisplay,
      activityFeed: state.data.activityFeed
  };
}
function mapDispatchToProps(dispatch) {
  return {
    updateUrlAction: bindActionCreators(updateUrl, dispatch),
    fetchDataSuccessAction: bindActionCreators(fetchDataSuccess, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
