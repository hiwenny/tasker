export const eventsLabel = {
  posted: 'POSTED A TASK',
  completed: 'COMPLETED',
  assigned: 'ASSIGNED',
  bid: 'BID ON',
  comment: 'COMMENTED ON',
  joined: 'SIGNED UP'
}