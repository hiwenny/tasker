// TODO: use selector for this kind of operations.

/**
 * Searches using task id, gets the associated task object from a list of reference data objects.
 * @param {Object} ID of task
 * @param {Array} List of task objects to be searched through
 * @return {Object} Task object that has corresponding ID
 */
export const getTaskObjectFromId = (id, tasksObjectList) => {
  return tasksObjectList.find(taskObject => taskObject.id === id)
}
// export const getTaskObjectFromId = (id, taskObjectList) => taskObjectList.find(taskObject => taskObject.id === id)

/**
 * The following is just a more specific use of getting values from the task object
 * @param {Number} id 
 * @param {Object} taskObjectList 
 */
export const getTaskNameFromId = (id, tasksObjectList) => getTaskObjectFromId(id, tasksObjectList).name
export const getTaskSlugFromId = (id, tasksObjectList) => getTaskObjectFromId(id, tasksObjectList).slug

/**
 * Searches using user id, gets the associated task object from a list of reference data objects.
 * Currently identical to taskObject getter, but best kept separate in case of future changes
 * @param {Object} ID of user
 * @param {Array} List of profile objects to be searched through
 * @return {Object} Profile object that has corresponding ID
 */
export const getProfilesObjectFromId = (id, profilesObjectList) => {
  return profilesObjectList.find(profileObject => profileObject.id === id)
}

/**
 * The following is just a more specific use of getting values from the task object
 * @param {Number} id 
 * @param {Object} taskObjectList 
 */
export const getProfileNameFromId = (id, tasksObjectList) => `${getProfilesObjectFromId(id, tasksObjectList).abbreviated_name} `
export const getProfileSlugFromId = (id, tasksObjectList) => getProfilesObjectFromId(id, tasksObjectList).slug