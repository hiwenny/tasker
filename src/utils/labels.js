// Potentiall we can use selectors for more complicated mappings
// And a factory if we do similar kinds of url compositions.
export const composeUserSlug = (slug) => `/users/${slug}`
export const composeTaskSlug = (slug) => `/tasks/${slug}`
