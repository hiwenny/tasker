import {
  getTaskObjectFromId

} from './mapper'

describe('mapper utils', () => {
  it('getTaskObjectFromId should get the object that has matching ID out of a list ', () => {
    expect(getTaskObjectFromId(1234, [{slug: 'example', id: 2222}, {slug: 'example', id: 1234}])).toEqual({slug: 'example', id: 1234})
    expect(getTaskObjectFromId(1111, [{slug: 'example', id: 2222}, {slug: 'example', id: 1234}])).toBe(undefined)
  })
})