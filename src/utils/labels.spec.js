import {  
  composeUserSlug,
  composeTaskSlug
} from './labels'

describe('labels utils', () => {
  it('should produce the expected labels', () => {
    expect(composeUserSlug('example')).toBe('/users/example')
    expect(composeTaskSlug('example')).toBe('/tasks/example')
  })
})