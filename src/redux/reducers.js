import { combineReducers } from 'redux';

// Split this file if more reducers needed
const currentDisplay = ( state={}, payload ) => {
  switch (payload.type) {
      case 'url':
          return { ...state, url: payload.url };
      default:
          return state;
  }
};

const data = ( state={}, payload ) => {
  switch (payload.type) {
      case 'success':
          return { ...state, activityFeed: payload.data };
      default:
          return state;
  }
};
const rootReducer = combineReducers({
  currentDisplay,
  data
});
export default rootReducer;