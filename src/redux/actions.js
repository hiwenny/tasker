export const updateUrl = url => {
  return {
      type: 'url',
      url
  };
}

export const fetchDataSuccess = data => {
  return {
    type: 'success',
    data
  }
}