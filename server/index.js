const express = require('express')
// const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 3001
const mocks = require('./mocks/activity_feed.json')

// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/api/mocks', (req, res) => {
  res.send(mocks)
})

app.listen(port, () => console.log(`Listening on port ${port}`))